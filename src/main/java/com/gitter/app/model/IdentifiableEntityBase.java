package com.gitter.app.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;

@MappedSuperclass
public abstract class IdentifiableEntityBase implements Serializable{

	private static final long serialVersionUID = -5180612933074649939L;

	@Id
	@JsonIgnore
	protected String id;
	
	public IdentifiableEntityBase() {
		this.id = UUID.randomUUID().toString();
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}
	

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}


	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof IdentifiableEntityBase)) {
			return false;
		}
		IdentifiableEntityBase other = (IdentifiableEntityBase) obj;
		return getId().equals(other.getId());
	}
	
	
}
