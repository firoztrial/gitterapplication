package com.gitter.app.model;

import java.util.Date;
import java.util.List;

public class RequestMessage<T> {

	
	private String source;
	private Date timestamp;
	private List<T> updates;
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public List<T> getUpdates() {
		return updates;
	}
	public void setUpdates(List<T> updates) {
		this.updates = updates;
	}

	

	
	
	
}
