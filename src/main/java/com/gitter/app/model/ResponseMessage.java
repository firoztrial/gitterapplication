package com.gitter.app.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonAutoDetect
public class ResponseMessage<T> {
	
	private String status;
	private List<T> updates;
	
	@JsonFormat(shape = JsonFormat.Shape.NUMBER)
	private Date timestamp=new Date();
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<T> getUpdates() {
		return updates;
	}
	public void setUpdates(List<T> updates) {
		this.updates = updates;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	

}
