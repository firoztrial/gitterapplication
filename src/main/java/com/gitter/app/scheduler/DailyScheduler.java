package com.gitter.app.scheduler;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.gitter.app.model.Customer;
import com.gitter.app.model.ResponseMessage;
import com.gitter.app.repository.CustomerRepository;
import com.gitter.app.util.DateUtil;

@Component
public class DailyScheduler {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	Environment environment;
	
	@Autowired
	CustomerRepository customerRepository;
	

	@Scheduled(cron = "${cron.scheduler.import.data}")
	public void getDataFromGitter() {
		LOGGER.info("Import Data from Service ");
		String serviceUrl = MessageFormat.format(environment.getProperty("client.service.url"), DateUtil.yesterdayWorkStartTime(),DateUtil.yesterdayWorkEndTime());
		ResponseEntity<ResponseMessage<Customer> > respEntity = restTemplate.exchange(
				serviceUrl,
				  HttpMethod.GET,
				  null,
				  new ParameterizedTypeReference<ResponseMessage<Customer>>(){});
		
		ResponseMessage<Customer>  actualResponse=respEntity.getBody();
		if(!actualResponse.getUpdates().isEmpty())
		customerRepository.saveAll(actualResponse.getUpdates());
	}


	
}


