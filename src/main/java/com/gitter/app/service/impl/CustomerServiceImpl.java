package com.gitter.app.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.gitter.app.model.Customer;
import com.gitter.app.repository.CustomerRepository;
import com.gitter.app.service.CustomerService;
import com.gitter.app.util.DateUtil;

@Component
public class CustomerServiceImpl implements CustomerService {

	
	@Autowired
	CustomerRepository customerRepository;
	
	@Override
	public void saveCustomers(List<Customer> customers) {
		if(!CollectionUtils.isEmpty(customers))
		customerRepository.saveAll(customers);

	}

	@Override
	public List<Customer> getCustomer(Date startDate, Date endDate) {
		List<Customer> customers=customerRepository.findAllByTimestampBetween(startDate, endDate);
		
		return customers;
	}

}
