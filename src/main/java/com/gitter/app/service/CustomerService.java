package com.gitter.app.service;

import java.util.Date;
import java.util.List;

import com.gitter.app.model.Customer;

public interface CustomerService {

	public void saveCustomers(List<Customer> customers);
	public List<Customer> getCustomer(Date startDate,Date endDate);
	
	
	
}
