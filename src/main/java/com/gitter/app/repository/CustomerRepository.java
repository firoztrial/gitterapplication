package com.gitter.app.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gitter.app.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, String>{
	
	List<Customer> findAllByTimestampBetween(Date start, Date end);

}
