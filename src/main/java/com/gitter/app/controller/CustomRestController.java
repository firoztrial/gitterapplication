package com.gitter.app.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gitter.app.aspect.DateRangeNotValid;
import com.gitter.app.model.Customer;
import com.gitter.app.model.RequestMessage;
import com.gitter.app.model.ResponseMessage;
import com.gitter.app.service.CustomerService;
import com.gitter.app.util.DateUtil;

@RestController
@RequestMapping("/api")
public class CustomRestController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomRestController.class);

	

	@Autowired
	CustomerService customerService;
	
	
	@RequestMapping(value = "/postdata", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Object> postData(@RequestBody RequestMessage<Customer> request) {
		
		
		customerService.saveCustomers(request.getUpdates());
		ResponseMessage<Object> response=new ResponseMessage<Object>();
		response.setStatus("Success");
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Content-Type", "application/json");
		return new ResponseEntity<>(response,responseHeaders,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getData", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getData(@RequestParam(value="start", required = true) String start,@RequestParam(value="end", required = true) String end) {
		
		
		Date startDate=DateUtil.stringToDate(start);
		Date endDate=DateUtil.stringToDate(start);
		if(startDate.getTime()>endDate.getTime())
			throw new DateRangeNotValid();
		List<Customer> customers=customerService.getCustomer(startDate,endDate);
		ResponseMessage<Customer> response=new ResponseMessage<Customer>();
		response.setStatus(customers.isEmpty()? "failure":"Success");
		response.setUpdates(customers); 
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Content-Type", "application/json");

		return new ResponseEntity<>(response,responseHeaders,HttpStatus.OK);
	}
}
