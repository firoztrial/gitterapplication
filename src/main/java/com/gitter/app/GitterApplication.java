package com.gitter.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableJpaRepositories("com.gitter.app.repository")
@EnableScheduling
public class GitterApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitterApplication.class, args);
	}
	
	@Bean(name="restTemplate")
	  public RestTemplate httpRestTemplate() {
	    RestTemplate restTemplate = new RestTemplate();
	    return restTemplate;
	  }
	
}

