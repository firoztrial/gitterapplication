package com.gitter.app.aspect;

import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.gitter.app.model.ResponseMessage;


@Aspect
@Component
@Order(0)
public class ResponseExceptionHandler {
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());


	@Around(value = "@within(org.springframework.web.bind.annotation.RestController)")
	public Object handleException(ProceedingJoinPoint jp) {
		ResponseMessage<Object> responseMessage = null;
		

		Object object = null;
		try {
			object = jp.proceed();
		}catch (Exception e) {
			
			LOGGER.error("Exception ",e);
			responseMessage = new ResponseMessage<Object>();
			responseMessage.setStatus("failure");
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.set("Content-Type", "application/json");

			return  new ResponseEntity<>(responseMessage, responseHeaders, HttpStatus.OK);
		} catch (Throwable e) {
			LOGGER.error("Throwable ",e);
			responseMessage = new ResponseMessage<Object>();
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.set("Content-Type", "application/json");
			responseMessage.setStatus("failure");
			return  new ResponseEntity<>(responseMessage, responseHeaders, HttpStatus.OK);
		} finally {
			

		}
		return object;
	}
}

