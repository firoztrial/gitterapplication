package com.gitter.app.aspect;

public class DateRangeNotValid extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6009318645525836615L;

	 public DateRangeNotValid() {
	        super("This is not valid DateRange");
	    }
	
}
