package com.gitter.app.aspect;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;



@Component
@Aspect
@Order(1)
public class LoggingAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAspect.class);

	@Before(value = "execution(* com.gitter.app.controller..*.*(..))")
	public void logBefore(JoinPoint jp) {

		LOGGER.info("Entering method " + getMethodName(jp) + " of " + jp.getTarget().getClass());
		Object[] params = jp.getArgs();
		for (Object param : params) {
			LOGGER.info("Request: {}", toJson(param));
		}
	}

	@AfterReturning(pointcut = "execution(* com.gitter.app.controller..*.*(..))", returning = "returningVal")
	public void logAfter(JoinPoint jp, Object returningVal) {
		LOGGER.info("Returning Value from " + getMethodName(jp) + " of " + jp.getTarget().getClass() + " in JSON: "
				+ toJson(returningVal));
	}

	@AfterThrowing(pointcut = "execution(* com.gitter.app.controller..*.*(..))", throwing = "ex")
	public void logAfterThrowing(JoinPoint jp, Exception ex) {
		LOGGER.error("Exception in method " + getMethodName(jp) + " of " + jp.getTarget().getClass(), ex);
	}

	private String getMethodName(JoinPoint jp) {
		MethodSignature methodSignature = (MethodSignature) jp.getSignature();
		return methodSignature.getName();
	}

	private String toJson(Object obj) {
		String json = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(obj);
		} catch (JsonGenerationException e) {
			LOGGER.error("Could not convert " + obj + " to JSON.");
		} catch (JsonMappingException e) {
			LOGGER.error("Could not convert " + obj + " to JSON.");
		} catch (IOException e) {
			LOGGER.error("Could not convert " + obj + " to JSON.");
		}
		return json;
	}
}

