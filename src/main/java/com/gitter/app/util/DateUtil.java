package com.gitter.app.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);
	
	private static final String WORK_START_TIME="9:00:00";
	private static final String WORK_END_TIME="18:00:00";
	
    public static Date stringToDate(String strdate)    		{   
        DateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmss");  
        Date date = null; ; 
        try
        {    		                
            date = (Date)formatter.parse(strdate);  
        } catch (ParseException e)
        {
        	LOGGER.error("ParseException",e);
        }
        return date;
}
    
    public static Date stringToDate(String strdate,String format)    		{   
        DateFormat formatter = new SimpleDateFormat(format);  
        Date date = null; ; 
        try
        {    		                
            date = (Date)formatter.parse(strdate);  
        } catch (ParseException e)
        {
        	LOGGER.error("ParseException",e);
        }
        return date;
}
    
    
    public static String getDateAsString(Date date, String format) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		SimpleDateFormat df = new SimpleDateFormat(format);
		return df.format(cal.getTime());
	}
    
    
    
    public static String yesterdayWorkStartTime() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);

        cal.setTime(stringToDate(getDateAsString(cal.getTime(),"yyyy-MM-dd")+" "+WORK_START_TIME, "yyyy-MM-dd HH:mm:ss"));
        return getDateAsString(cal.getTime(),"yyyyMMddhhmmss");
    }
    
    public static String yesterdayWorkEndTime() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);

        cal.setTime(stringToDate(getDateAsString(cal.getTime(),"yyyy-MM-dd")+" "+WORK_END_TIME, "yyyy-MM-dd HH:mm:ss"));
        return getDateAsString(cal.getTime(),"yyyyMMddhhmmss");
    }

}
