package com.gitter.app.controller;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import com.gitter.app.model.Customer;
import com.gitter.app.model.RequestMessage;
import com.gitter.app.service.impl.CustomerServiceImpl;
public class CustomerRestControllerTest {

	private IMocksControl mockControl;
    private CustomRestController testObj;
    private CustomerServiceImpl mockCustomerService;
   
    @Before
    public void setUp() throws Exception {
        mockControl = EasyMock.createControl();
        mockCustomerService=mockControl.createMock(CustomerServiceImpl.class);
        testObj = new CustomRestController();
       
    }
    
    
	@Test
	public void testPostData() throws Exception {
		
		RequestMessage<Customer> mockPostDataReq = EasyMock.createMock(RequestMessage.class);
		testObj.postData(mockPostDataReq);
		EasyMock.expectLastCall().andThrow(new RuntimeException("error"));
	}
    
}



	


