package com.gitter.app.aspect;

import javax.servlet.http.HttpSession;

import org.aspectj.lang.ProceedingJoinPoint;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

public class ResponseExceptionHandlerTest {

	private ResponseExceptionHandler testObj;
	private ProceedingJoinPoint jp;

	
    @Before
    public void setUp() throws Exception {
    	jp = EasyMock.createMock(ProceedingJoinPoint.class);
    	testObj = new ResponseExceptionHandler();
    }
    
	@Test
	public void testHandleException() {
		testObj.handleException(jp);
	}

}
